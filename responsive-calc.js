if (!window.console) console = { log: function() {}, warn: function() {}, info: function() {}, error: function() {}, debug: function() {} };

/**
 * Use like this:
 * 
 * $(".no-csscalc .mycolumn").calcBehavior({
 * 	  what : 'width',
 *  	percent : 1,	
 *  	additionnal : -50
 * });
 * 
 */
(function( $ ) {
	$.fn.calcBehavior = function(options) {

		var settings = $.extend( {
			what : 'width',
	   	percent : 1,			// 0 to 1
	   	additionnal : -10
	  }, options);

	  return this.each(function() {
		  var $this = $(this);

		  function _calc(){
		  	var containerWidth = $this.parent().innerWidth();
		  	var variablePart = containerWidth * settings.percent;
				var finalWidth = variablePart + settings.additionnal;
				//console.log('  [calcBehavior] containerWidth=' + containerWidth + ' ' + settings.percent*100 + '% ' + settings.additionnal + '  ==> ' + finalWidth);
				$this.css(settings.what, finalWidth + 'px');
		  }
			_calc();


		});

	};


})( jQuery );

$(function() {
	Modernizr.addTest('csscalc', function() {
		var prop = 'width:';
		var value = 'calc(10px);';
		var el = document.createElement('div');

		el.style.cssText = prop + Modernizr._prefixes.join(value + prop);

		return !!el.style.length;
	});
});
